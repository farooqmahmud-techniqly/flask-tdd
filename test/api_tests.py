import unittest
from flask import json
from src.app import *
from datetime import datetime


class ApiTests(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()

    def tearDown(self):
        pass

    def test_ping_returns_ok_with_response(self):
        response = self.app.get("/api/ping")
        self.assertEqual(response.status_code, 200)

        content = json.loads(response.data)
        self.assertEqual(content["version"], "1")
        self.assertGreaterEqual(int(datetime.utcnow().timestamp()), content["ts"])


if __name__ == "__main__":
    unittest.main()
