from flask import Flask
from flask import make_response
from flask import jsonify
from datetime import  datetime

app = Flask(__name__)


@app.route("/api/ping")
def ping():
    return make_response(jsonify({"version": "1", "ts": int(datetime.utcnow().timestamp())})), 200


if __name__ == "__main__":
    app.run(debug=True, port=7777)
